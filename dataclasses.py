'''
Created on 10.06.2013

@author: franzi
'''
from __future__ import print_function
from sqlite3 import Cursor
import datetime

class Country(object):

    def __init__(self):  # konstruktor
        self.token = ""
        self.name = ""
        
    def insert_into_database(self, cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute("INSERT OR IGNORE INTO countries VALUES (?, ?)", [self.name, self.token])
        
    @staticmethod
    def create_table(cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute('''DROP TABLE IF EXISTS countries''')
        cursor.execute('''CREATE TABLE countries (name TEXT NOT NULL, token TEXT PRIMARY KEY)''')
        
    @staticmethod
    def from_line(line):
        new_object = Country()
        new_object.token = line[0:2]
        new_object.name = line[4:-1].strip()
        return new_object
   
   
class Networks(object):

    def __init__(self):  # konstruktor
        self.token = ""
        self.name = ""
        
    def insert_into_database(self, cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute("INSERT OR IGNORE INTO networks VALUES (?, ?)", [self.name, self.token])
        
    @staticmethod
    def create_table(cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute('''DROP TABLE IF EXISTS networks''')
        cursor.execute('''CREATE TABLE networks (name TEXT NOT NULL, token TEXT PRIMARY KEY)''')
   
   
class Station(object):

    def __init__(self):
        self.id = 0
        self.name = ""
        self.latitude = 0.0
        self.longitude = 0.0
        self.elevation = 0
        self.contries_token = ""
        self.country = None
        self.networks = []
        
    @staticmethod
    def from_database(id_, countries=None): #countries ist optionaler parameter
        pass

    def insert_into_database(self, cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute("INSERT INTO stations VALUES (?, ?, ?, ?, ?, ?)", [self.id, self.name, self.latitude, self.longitude, self.elevation, self.contries_token])
        for network in self.networks:
            cursor.execute("INSERT INTO linkingTableStationNetwork VALUES (?, ?, ?)", [None, network, self.id])
        
    @staticmethod
    def create_table(cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute('''DROP TABLE IF EXISTS stations''')
        cursor.execute('''CREATE TABLE stations ( id INTEGER PRIMARY KEY, name TEXT NOT NULL, latitude REAL NOT NULL, longitude REAL NOT NULL, elevation INTEGER NOT NULL, fk_countries_token TEXT NOT NULL,FOREIGN KEY(fk_countries_token) REFERENCES countries(token))''')
        cursor.execute('''DROP TABLE IF EXISTS linkingTableStationNetwork''')
        cursor.execute('''CREATE TABLE linkingTableStationNetwork ( id INTEGER PRIMARY KEY AUTOINCREMENT, networkToken TEXT NOT NULL, stationID INTEGER NOT NULL, FOREIGN KEY(networkToken) REFERENCES networks(token), FOREIGN KEY(stationID) REFERENCES stations(id))''')
        
    @staticmethod
    def from_line(line):
        new_object = Station()
        new_object.contries_token = line[0:2]
        new_object.id = line[4:9]
        new_object.name = line[11:46].strip()
        new_object.latitude = line[47:53].strip()
        new_object.longitude = line[54:61].strip()
        new_object.elevation = line[62:66].strip()
        
        if line[67:68] != " ":
            new_object.networks.append(line[67:68])
        if line[68:69] != " ":
            new_object.networks.append(line[68:69])

        return new_object

    @staticmethod
    def get_all_station_ids(cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute('''SELECT id FROM stations''')
        return set(res[0] for res in cursor.fetchall())
    
    @staticmethod
    def get_network_and_country(cursor, stationID):
        assert isinstance(cursor, Cursor)
        print(stationID)
        cursor.execute('''SELECT countries.name, networks.name FROM stations JOIN countries ON stations.fk_countries_token = countries.token LEFT JOIN linkingTableStationNetwork ON stations.id = linkingTableStationNetwork.stationID LEFT JOIN networks ON linkingTableStationNetwork.networkToken = networks.token WHERE stations.id = ?''', [stationID])
        return cursor.fetchall() 
        
    @staticmethod
    def get_all_station_names_with_records(cursor):
        assert isinstance(cursor, Cursor)
#zu langsam      cursor.execute('''SELECT DISTINCT s.name AS stationname, stations.id AS stationid FROM records r JOIN stations s ON r.stationID = s.id ORDER BY stationname''')
        cursor.execute('''SELECT stations.name, stations.id, countries.name FROM stations  JOIN countries ON countries.token = stations.fk_countries_token WHERE id in (SELECT DISTINCT stationID FROM records)''')
        result_set = cursor.fetchall()
        return result_set
    
class Record(object):
    
    def __init__(self):
        self.id = 0
        self.id_station = 0
        self.date = None
        self.pressure_lvls = []
       
    def insert_into_database(self, cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute("INSERT INTO records VALUES (?, ?, ?)", [None, self.id_station, self.date])
        cursor.execute('''SELECT id FROM records WHERE ROWID = ?''',[cursor.lastrowid])
        self.id = cursor.fetchone()[0]
        
    @staticmethod
    def create_table(cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute('''DROP TABLE IF EXISTS records''')
        cursor.execute('''CREATE TABLE records ( id INTEGER PRIMARY KEY AUTOINCREMENT, stationID INTEGER NOT NULL, date TEXT NOT NULL, FOREIGN KEY(stationID) REFERENCES stations(id))''')
        cursor.execute('''CREATE INDEX "sid_idx" on records (stationID)''')
    @staticmethod
    def from_header_line(line):
        new_object = Record()
        new_object.id_station = line[1:6]
        year = int(line[6:10])
        month = int(line[10:12])
        day = int(line[12:14])
        hour = int(line[14:16])
        new_object.date = datetime.datetime(year, month, day, hour)

        return new_object

    def add_pressurelvl_entry_from_line(self, line):
        pressure_lvl = PressureLvl.from_line(line, self.id)
        self.pressure_lvls.append(pressure_lvl)
    
        return pressure_lvl
    
    @staticmethod
    def get_records(station_id, cursor):
            assert isinstance(cursor, Cursor)
            cursor.execute('''SELECT records.date, MAX(pressurelvl.temperature) FROM records JOIN pressurelvl ON records.id  = pressurelvl.recordID AND records.stationID =  ? GROUP BY records.date''', [station_id])
            result_set = cursor.fetchall()

            return result_set
    
    
class PressureLvl(object):
    
    def __init__(self):
        self.id = 0
        self.lvl = 0
        self.id_record = 0
        self.temperature = 0
        
    @staticmethod
    def from_line(line, id_record):
        new_object = PressureLvl()
        new_object.lvl = line[0:7]
        new_object.id_record = id_record
        new_object.temperature = line[24:31]

        return new_object
    
    def insert_into_database(self, cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute("INSERT INTO pressurelvl VALUES (?, ?, ?, ?)", [None, self.id_record, self.lvl, self.temperature])
    
    @staticmethod
    def create_table(cursor):
        assert isinstance(cursor, Cursor)
        cursor.execute('''DROP TABLE IF EXISTS pressurelvl''')
        cursor.execute('''CREATE TABLE pressurelvl ( id INTEGER PRIMARY KEY AUTOINCREMENT, recordID INTEGER NOT NULL, lvl INTEGER NOT NULL, temperature INTEGER NOT NULL, FOREIGN KEY(recordID) REFERENCES records(id))''')
        cursor.execute('''CREATE INDEX "rid_idx" on pressurelvl (recordID)''')
    
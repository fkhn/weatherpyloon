# WeatherPyloon

A program for importing and visualising weather data from NOAA. When all Data is imported, you can search your database for a weather-station and see a diagram with temperature recorded at ground-level.

## Used Data

* weather data, recorded by weather balloons
* Origin of Data: NOAA (http://www.ncdc.noaa.gov/)
* download from: ftp://ftp.ncdc.noaa.gov/pub/data/igra/derived-v2/ (anonymous ftp, there is also a readme with all information about the files and data)

## Prerequisites

* sqlite3 for python
* matplotlib for python
* gzip for python
* numpy for python

## How to use

* copy the required files and folder for importing in .../WeatherPyloonAnalysis/data
* the required files and folder are:
    * derived-countries.txt (file)
    * derived-stations.txt (file)
    * data-por (folder)

* run for importing: **Importer.py** (if you want to import all data from source, it will take many hours...)
* run for searching for a weather-station and showing the diagram: **UserInterface.py**

* the modul **dataclasses.py** contains the dataclasses and most database transactions
* the modul **diagram.py** contains the construction of the diagrams
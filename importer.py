'''
Created on 10.06.2013

@author: franzi
'''
from __future__ import print_function
import os
import dataclasses
import sqlite3
import gzip
from dataclasses import Station

if __name__ == '__main__':
    print(("=" * 15) + " Welcome to the Weather Pyloon Importer "+ ("=" * 15)+"\n") 
   
    
    folder = os.path.join(os.path.dirname(os.path.realpath(__file__)),"data")
    country_file = os.path.join(folder, "derived-countries.txt")
    station_file = os.path.join(folder, "derived-stations.txt")
    record_file = os.path.join(folder, "data-por")
    
    print("At first, I will create tables:")   
     
    connection = sqlite3.connect(os.path.join(folder, "weatherPyloon.db"))
    cursor = connection.cursor()
    
    print("countries...", end="")
    
    dataclasses.Country.create_table(cursor)
    with open(country_file) as f:
        for line in f:
            dataclasses.Country.from_line(line).insert_into_database(cursor)
    
    print("done.")
    print("networks...", end="")
    
    dataclasses.Networks.create_table(cursor)
    
    station1 = dataclasses.Networks()
    station1.name = "GCOS Upper Air Network (GUAN)"
    station1.token = "G"
    station1.insert_into_database(cursor)
    
    station2 = dataclasses.Networks()
    station2.name ="Former GCOS Upper Air Network (GUAN)"
    station2.token = "F"
    station2.insert_into_database(cursor)
    
    station3 = dataclasses.Networks()
    station3.name = "Lanzante-Klein-Seidel (LKS) station network"
    station3.token = "L"
    station3.insert_into_database(cursor)
    
    print("done.")
    print("stations...", end="")
    
    dataclasses.Station.create_table(cursor)    
    with open(station_file) as f:
        for line in f:
            dataclasses.Station.from_line(line).insert_into_database(cursor)
    
    print("done.\n")
    print("records...")
    
    dataclasses.Record.create_table(cursor)
    dataclasses.PressureLvl.create_table(cursor)
    
    print("get_all...")
    all_stations = Station.get_all_station_ids(cursor)
    print("done.")
    for count, file_name in enumerate(all_stations):
        print(str(count+1) + " of " + str(len(all_stations)), end = ": ")
        p = os.path.join(record_file, str(file_name).zfill(5) + ".dat.gz")
        if os.path.isfile(p):
            print("reading station " + str(file_name) +"...", end = " ")
            with gzip.open(p) as f:
                
                try:
                    record = None
                    for line in f:
                        if line.startswith("#"):
                            record = dataclasses.Record.from_header_line(line)
                            record.insert_into_database(cursor)
                        else:
                            record.add_pressurelvl_entry_from_line(line).insert_into_database(cursor)
                            
                except IOError:
                    print("Error reading file", end = "")
                finally:
                    print("")
        else:
            print("station " + str(file_name) + " has got no records.")
    print("done.\n")
    print("And now I commit and close...", end="")
    
    connection.commit()
    connection.close()
    
    print("finished.\n")
    print("Goodby!")
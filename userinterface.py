'''
Created on 17.06.2013

@author: franzi
'''
from __future__ import print_function
import sys
import os
import sqlite3
import dataclasses
import diagram


def clear_terminal():
    os.system('clear')
    print(("=" * 15) + " Welcome to the Weather Pyloon User Interface "+ ("=" * 15)+"\n")

def searchStationName(stations_set):
    user_input = raw_input("search (station): ")
    result = list(entry for entry in stations_set if user_input.lower() in entry[0].lower())
    
    if len(result) > 1:
        print(result)
    elif len(result) == 0:
        print("nothing found")
    elif len(result) == 1:
        diagram.create_diagram(result[0][1], result[0][0])
    
    user_input = raw_input("Do You want to search for another station?(y/N)")
    if user_input == "y":
        clear_terminal()
        searchStationName(stations_set)


if __name__ == '__main__':
    clear_terminal()
    
    print("I am creating a list of stations with records...", end="")
    sys.stdout.flush()
    connection = sqlite3.connect(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"data"), "weatherPyloon.db"))
    cursor = connection.cursor()
    stations_set = dataclasses.Station.get_all_station_names_with_records(cursor)
    connection.close()
    print(" done \n")
    
    searchStationName(stations_set)
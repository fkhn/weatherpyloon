'''
Created on 17.06.2013

@author: franzi
'''
import os
import datetime
import matplotlib.pyplot as plt
import sqlite3
import dataclasses
from numpy import average


def moving_average(list_, days):
    c = list(list_)
    r = []
    while len(c) > 0:
        r.append(c.pop(0))
        if len(r) > days:
            r.pop(0)
        yield average(r)

def create_diagram(stationID, station_name):
    connection = sqlite3.connect(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),"data"), "weatherPyloon.db"))
    cursor = connection.cursor()
    data_set = dataclasses.Record.get_records(stationID, cursor)
    info_set = dataclasses.Station.get_network_and_country(cursor, stationID)
    connection.close()
        
    information = str(info_set[0][0]) + ' -'
    for info in info_set:
        information = information + " " + str(info[1]) + ","
    information = information[:-1]

    list_a = list(datetime.datetime.strptime(res[0][0:10], "%Y-%m-%d") for res in data_set)
    list_b = list((res[1]/10 -273) for res in data_set)

    fig = plt.figure(str(station_name))
    fig.suptitle(information)
    ax = fig.add_subplot(1,1,1)
    ax3 = fig.add_subplot(1,1,1)
    ax.plot(list_a, list_b, ".")
    ax3.plot(list_a, list(moving_average(list_b, 7)))
    ax.grid(True)
    plt.show()

    